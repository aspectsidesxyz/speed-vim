local function map(m, k, v)
	vim.keymap.set(m, k, v, { silent = true })
end

-- Mimic shell movements
map("i", "<C-E>", "<ESC>A")
map("i", "<C-A>", "<ESC>I")

-- Load recent sessions
map("n", "<leader>sl", "<CMD>SessionLoad<CR>")

-- Undo Tree
map("n", "<leader>u", "<CMD>UndotreeToggle<CR>")

-- Keybindings for telescope
map("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>")
map("n", "<leader>ff", "<CMD>Telescope find_files<CR>")
map("n", "<leader>fb", "<CMD>Telescope file_browser<CR>")
map("n", "<leader>fw", "<CMD>Telescope live_grep<CR>")
map("n", "<leader>ht", "<CMD>Telescope colorscheme<CR>")
map('n', '<C-p>', "<CMD>Telescope git_files<CR>")

-- Keybindings for Harpoon
map("n", "<leader>a", "<CMD>lua require('harpoon.mark').add_file()<CR>")
map("n", "<C-e>", "<CMD>lua require('harpoon.ui').toggle_quick_menu()<CR>")



-- Keybinding for Tree
map('n', '<leader>e', ':NvimTreeToggle<CR>')

-- Fugitive 
map('n', "<leader>gs", ':Git<CR>')
